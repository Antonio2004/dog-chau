//Librerias 

#include <Arduino.h>
#include "HX711.h"
#include <TimerOne.h>

//Probando save de inicio día 2
//Probando save día 3
//Probando día 4
//Probando día 6
//Probando día 7
//Probando día 8
//Día de entrega 
//Prueba del profe
//prueba mía

//pines control

const int Electrovalvula = 1;
const int Motor = 2;

// Pines Analogicos de la Balanza

const int DOUTC = A1;
const int CLKC = A2;
const int DOUTA = A3;
const int CLKA = A4;

// Led de señal de espera

const int LEDProceso = 10;

//Botones de Configuración

const int Boton_UP = 7;
const int Boton_Down = 8;
const int Boton_SetReset = 9;

//Balanzas

HX711 BalanzaC;
HX711 BalanzaA;

// Variables de Menu 

int Tamaño = 2;
bool EncendidoApagado = 0;


//numeros binarios para 7 segmentos

const int DisplayA = 3;
const int DisplayB = 4;

// Guardado de peso 

float Agua;
float Comida;

// Tiepo de servir 

int tiempo;

// declaro función de interrupciíon para el timer 

void servir(int);

// Controlador de dejar de servir comida 

bool PararC = false;

void setup() {

// INICIO Set de Electrovalvula y Motor de 12 Vcc

pinMode(Electrovalvula, OUTPUT);
pinMode(Motor, OUTPUT);

// INICIO Set botones de configuración

pinMode(Boton_UP, INPUT);
pinMode(Boton_Down, INPUT);
pinMode(Boton_SetReset, INPUT);

//INICIO Configuración de Dysplay 7 Segmentos

pinMode(DisplayA, OUTPUT);
pinMode(DisplayB, OUTPUT);

// INICIO Configuración de las Balanzas

pinMode ( LEDProceso , OUTPUT ); 
digitalWrite( LEDProceso , HIGH ); // Señal de Inicio de Configuración  
BalanzaC.begin( DOUTC , CLKC );
BalanzaA.begin( DOUTA, CLKA );
BalanzaC.set_scale( 439430.25 );// Nesesita mediciones de testeo
BalanzaA.set_scale( 439430.25 );// Nesesita mediciones de testeo 
BalanzaC.tare( 20 );
BalanzaA.tare( 20 );
digitalWrite(LEDProceso, LOW); // Señal de FIN de Configuración


}

void loop() {

// inicio del reloj

Timer1.initialize(tiempo);

// Estado de Configuración

if (EncendidoApagado == 0)
{

    //Deshabilitar el timer

    Timer1.detachInterrupt();

    //Cambiar tamaño del perro 


    if( digitalRead(Boton_UP) == 1 ){
        Tamaño++;
        
    }
    else if ( digitalRead(Boton_Down == 1)){
        Tamaño--;
    }

    //Regulación 

    if ( Tamaño > 3 )
    {
        Tamaño = 3;
    }
    else if (Tamaño < 1)
    {
        Tamaño = 1;
    }
    
    // Muestra en Display 7 segmentos 
    if (Tamaño == 1)
    {
        digitalWrite( DisplayA, HIGH);
        digitalWrite( DisplayB, LOW);
        tiempo=17280000000; // 5 veces al día / 4,8 hs
    }
    else if (Tamaño == 2)
    {
        digitalWrite( DisplayA, LOW);
        digitalWrite( DisplayB, HIGH);
        tiempo = 28800000000; // 3 veces al día / 8hs
    }
    else if (Tamaño == 3)
    {
        digitalWrite( DisplayA, HIGH);
        digitalWrite( DisplayB, HIGH);
        tiempo = 43200000000; // 2 veces al día / 12 hs
    }
    if(Boton_SetReset == 1 ){
        EncendidoApagado = 1;
        Timer1.initialize(tiempo);
    }
    

}

// Estado de Funcionamiento 

else if(EncendidoApagado == 1)
{

    //Medición de balanzas 
    Agua = BalanzaA.get_value();
    Comida = BalanzaC.get_value();

    //Habilitar el timer

    Timer1.attachInterrupt(servircomida);

    //Parar el servir comida

    if (Tamaño == 3 && Comida > 400 && PararC == true) {
        servircomida;
    }
    else if(Tamaño == 2 && Comida > 300 && PararC == true){
        servircomida;
    }
    else if(Tamaño == 1 && Comida > 200 && PararC == true){
        servircomida;
    }

    //Servir agua 

    if(Agua < 400 && PararC == true){
        digitalWrite(Electrovalvula, HIGH);
        delay(10);
        digitalWrite(Electrovalvula, LOW);
    }

    if(Agua > 400 && PararC == false){
        digitalWrite(Electrovalvula, HIGH);
        delay(10);
        digitalWrite(Electrovalvula, LOW);
    }

}
}

void servircomida(void){

    PararC = !PararC;
    digitalWrite(Motor, HIGH);
    delay(10);
    digitalWrite(Motor, LOW);

}  